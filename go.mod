module gitee.com/wheat-os/grpc-proxy

go 1.17

require (
	github.com/mwitkow/grpc-proxy v0.0.0-20181017164139-0f1106ef9c76
	golang.org/x/net v0.0.0-20211011170408-caeb26a5c8c0
	google.golang.org/grpc v1.41.0
)

require (
	github.com/golang/protobuf v1.4.3 // indirect
	golang.org/x/sys v0.0.0-20210423082822-04245dca01da // indirect
	golang.org/x/text v0.3.6 // indirect
	google.golang.org/genproto v0.0.0-20200526211855-cb27e3aa2013 // indirect
	google.golang.org/protobuf v1.25.0 // indirect
)
